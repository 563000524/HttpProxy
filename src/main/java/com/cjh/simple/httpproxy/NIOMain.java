package com.cjh.simple.httpproxy;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.cjh.simple.httpproxy.util.TRACEID;

/**
 * nio方式的代理服务器,可以运行，目前没有发现什么bug
 *
 */
public class NIOMain
{
    private static Logger            logger = LogManager.getLogger(LogManager.ROOT_LOGGER_NAME);
    public static ThreadPoolExecutor pool   = (ThreadPoolExecutor) Executors.newCachedThreadPool();
    
    @SuppressWarnings("resource")
    public static void main(String[] args) throws IOException
    {
        Selector selector = Selector.open();
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.configureBlocking(false);
        serverSocketChannel.socket().bind(new InetSocketAddress(8888));
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT, "a");
        while (true)
        {
            if (selector.select() > 0)
            {
                continue;
            }
            Set<SelectionKey> selectedKeys = selector.selectedKeys();
            Iterator<SelectionKey> iterator = selectedKeys.iterator();
            while (iterator.hasNext())
            {
                SelectionKey next = iterator.next();
                try
                {
                    if (next.isAcceptable())
                    {
                        // 收到服务端消息
                        SocketChannel socketChannel = serverSocketChannel.accept();
                        if (socketChannel != null)
                        {
                            socketChannel.configureBlocking(false);
                            socketChannel.register(selector, SelectionKey.OP_READ, "a");
                        }
                    }
                    else if (next.isReadable())
                    {
                        Object key = next.attachment();
                        // 第一次连接
                        if (key instanceof String)
                        {
                            SocketChannel fromChannel = (SocketChannel) next.channel();
                            ByteBuffer buffer = ByteBuffer.allocate(1024 * 10);
                            int readSize = fromChannel.read(buffer);
                            if (readSize == 0)
                            {
                                continue;
                            }
                            else if (readSize == -1)
                            {
                                logger.debug("traceId:{},关闭连接{}", TRACEID.currentTraceId(), fromChannel.getLocalAddress());
                                next.cancel();
                                if (next.channel() != null)
                                {
                                    next.channel().close();
                                }
                                continue;
                            }
                            else
                            {
                                byte[] bytes = buffer.array();
                                String header = new String(bytes, 0, bytes.length);
//                                System.out.println(header);
                                // 解析请求头
                                Info info = getServer(header);
                                pool.execute(new Initer(fromChannel, info, next, header));
                            }
                        }
                        // 如果object不为String，则说明携带的是数据接收方，所以读取数据->发送到数据接收方
                        else
                        {
                            SocketChannel fromChannel = (SocketChannel) next.channel();
                            SocketChannel toChannel = (SocketChannel) key;
                            ByteBuffer b = ByteBuffer.allocate(1024 * 100);
                            int readSize = fromChannel.read(b);
                            if (readSize == 0)
                            {
                                continue;
                            }
                            else if (readSize == -1)
                            {
                                logger.debug("traceId:{},关闭连接{}", TRACEID.currentTraceId(), fromChannel.getLocalAddress());
                                next.cancel();
                                if (next.channel() != null)
                                {
                                    next.channel().close();
                                }
                                continue;
                            }
                            else
                            {
                                b.flip();
                                while (b.hasRemaining())
                                {
                                    toChannel.write(b);
                                }
                                b.clear();
                                logger.debug("traceId:{},发送数据{}", TRACEID.currentTraceId(), readSize);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    next.cancel();
                    if (next.channel() != null)
                    {
                        try
                        {
                            next.channel().close();
                        }
                        catch (Exception e2)
                        {
                        }
                    }
                }
                finally
                {
                    iterator.remove();
                }
            }
        }
    }
    
    static class Initer implements Runnable
    {
        private SocketChannel fromChannel;
        private Info          info;
        private String        header;
        private SelectionKey  key;
        
        public Initer(SocketChannel socketChannel, Info info, SelectionKey key, String header)
        {
            this.fromChannel = socketChannel;
            this.info = info;
            this.key = key;
            this.header = header;
        }
        
        @Override
        public void run()
        {
            try
            {
                String CRLF = "\r\n";
                SocketChannel toChannel = SocketChannel.open();
                toChannel.configureBlocking(false);
                toChannel.connect(new InetSocketAddress(info.getServerAddr(), info.getPort()));
                // 等待socket创建完成
                int num = 0;
                while (!toChannel.finishConnect())
                {
                    Thread.sleep(500);
                    num++;
                    if (num > 60)
                    {
                        fromChannel.close();
                        return;
                    }
                }
                toChannel.register(key.selector(), SelectionKey.OP_READ, fromChannel);
                key.attach(toChannel);
                logger.debug("traceId:{},创建连接{},{},{}", TRACEID.currentTraceId(), info.getServerAddr(), info.getPort(), info.getReqMethod());
                // Connect请求直接返回成功
                if (info.getReqMethod().equalsIgnoreCase("CONNECT"))
                {
                    String proxyResponse = "HTTP/1.1 200 Connection Established" + CRLF + "Proxy-agent:Anyone-BlindProxyServer/1.0" + CRLF + CRLF;
                    ByteBuffer buf = ByteBuffer.allocate(proxyResponse.getBytes().length);
                    buf.clear();
                    buf.put(proxyResponse.getBytes());
                    buf.flip();
                    // System.out.println("发送CONNECT确认消息");
                    while (buf.hasRemaining())
                    {
                        fromChannel.write(buf);
                    }
                }
                // 否则直接中转数据
                else
                {
                    ByteBuffer buf = ByteBuffer.allocate(header.getBytes().length);
                    buf.clear();
                    buf.put(header.getBytes());
                    buf.flip();
                    // System.out.println("发送Header消息");
                    while (buf.hasRemaining())
                    {
                        toChannel.write(buf);
                    }
                }
            }
            catch (Exception e)
            {
                ;
            }
        }
        
    }
    
    static class Info
    {
        private String serverAddr;
        private int    port;
        private String reqMethod;
        
        public String getServerAddr()
        {
            return serverAddr;
        }
        
        public void setServerAddr(String serverAddr)
        {
            this.serverAddr = serverAddr;
        }
        
        public int getPort()
        {
            return port;
        }
        
        public void setPort(int port)
        {
            this.port = port;
        }
        
        public String getReqMethod()
        {
            return reqMethod;
        }
        
        public void setReqMethod(String reqMethod)
        {
            this.reqMethod = reqMethod;
        }
        
    }
    
    private static Info getServer(String header)
    {
        String CRLF = "\r\n";
        String serverAddr = null;
        int serverPort = 80;
        // 默认只读取前100K,假定所有的头部都不超过100K就可以解析到Host头部.这块也是不好的地方
        // 获取请求行,根据HTTP协议规范,第一行为请求行.
        String input = header;
        if (input.indexOf(CRLF) == -1)
        {
            Info info = new Info();
            info.setReqMethod("aaaaa");
            return info;
        }
        String reqLine = input.substring(0, input.indexOf(CRLF));
        logger.debug("traceId:{},请求：{}", TRACEID.currentTraceId(), reqLine);
        String[] params = reqLine.split(" ");
        String reqMethod = params[0];
        String reqUrl = params[1];
        String transferUrl = getUrl(input, reqUrl);
        if (transferUrl.indexOf(":") > 0)
        {
            serverAddr = transferUrl.substring(0, transferUrl.indexOf(":"));
            serverPort = Integer.parseInt(transferUrl.substring(transferUrl.indexOf(":") + 1));
        }
        else
        {
            serverAddr = transferUrl;
            serverPort = 80;
        }
        Info info = new Info();
        // info.setPort(60000);
        // info.setServerAddr("127.0.0.1");
        // info.setReqMethod("GET");
        info.setPort(serverPort);
        info.setServerAddr(serverAddr);
        info.setReqMethod(reqMethod);
        return info;
    }
    
    private static String getUrl(String input, String reqUrl)
    {
        String CRLF = "\r\n";
        String serverUrl = null;
        // 解析远端主机.
        if (reqUrl.startsWith("/"))
        {
            int location = 0;
            while ((location = input.indexOf(CRLF)) > 0)
            {
                String header = input.substring(0, location);
                // 根据HTTP协议规范,如果请求行中使用相对路径,则Web Server主机由Host头部决定.
                // 需要注意HTTP协议规范中是大小写不敏感的,所以需要使用equalsIgnoreCase
                if (header.length() > 5 && header.substring(0, 5).equalsIgnoreCase("Host:"))
                {
                    String host = header.substring("Host:".length(), header.length());
                    serverUrl = host;
                    break;
                }
                input = input.substring(location + CRLF.length());
            }
        }
        else
        {
            // 根据HTTP协议规范,如果请求行中使用绝对路径,则Web
            // Server主机是URL的一部分,且必须忽略Host头部.
            serverUrl = reqUrl;
        }
        // url: http://www.baidu.com/index.html
        if (serverUrl.indexOf("//") > 0)
        {
            serverUrl = serverUrl.substring(serverUrl.indexOf("//") + 2);
        }
        // 去除URL中的路径只保留域名: www.baidu.com/index.html
        if (serverUrl.indexOf("/") > 0)
        {
            serverUrl = serverUrl.substring(0, serverUrl.indexOf("/"));
        }
        return serverUrl;
    }
}
