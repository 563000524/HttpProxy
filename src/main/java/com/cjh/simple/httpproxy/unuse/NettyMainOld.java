package com.cjh.simple.httpproxy.unuse;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.cjh.simple.httpproxy.util.TRACEID;

import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.AdaptiveRecvByteBufAllocator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.util.AttributeKey;
import lombok.Data;

/**
 * 2018年9月17日14:25:33 尝试使用Netty方式实现代理服务器
 *
 */
public class NettyMainOld {
	private static Logger logger = LogManager.getLogger(LogManager.ROOT_LOGGER_NAME);
	private static NioEventLoopGroup group = new NioEventLoopGroup(100);
	private static NioEventLoopGroup group2 = new NioEventLoopGroup(100);
	private static ServerBootstrap serverBootstrap = new ServerBootstrap();
	private static Bootstrap clientBootstrap = new Bootstrap();
	private static final AttributeKey<Info> CONTEXT = AttributeKey.valueOf("context");
	private static Set<String> whiteList = new HashSet<>(Arrays.asList("127.0.0.1", "0:0:0:0:0:0:0:1", "58.22.61.221"));

	public static void main(String[] args) throws InterruptedException {
		try {
			final IPInterceptor interceptor = new IPInterceptor();
			clientBootstrap.group(group2).option(ChannelOption.TCP_NODELAY, true).channel(NioSocketChannel.class)//
					.option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT) //
					.option(ChannelOption.TCP_NODELAY, true)//
					.option(ChannelOption.RCVBUF_ALLOCATOR, new AdaptiveRecvByteBufAllocator(65536, 65536, 65536))//
					.handler(new ChannelInitializer<SocketChannel>() {

						@Override
						protected void initChannel(SocketChannel ch) throws Exception {
							ch.pipeline()//
									.addLast(new IdleStateHandler(300, 300, 300))//
									.addLast(new ChannelB());
						}

					});
			serverBootstrap.group(group, group).channel(NioServerSocketChannel.class)//
					.option(ChannelOption.SO_BACKLOG, 1024)//
					.option(ChannelOption.TCP_NODELAY, true)//
					.option(ChannelOption.RCVBUF_ALLOCATOR, new AdaptiveRecvByteBufAllocator(65536, 65536, 65536))//
					.option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT) //
					.childOption(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT)//
					.childOption(ChannelOption.RCVBUF_ALLOCATOR, new AdaptiveRecvByteBufAllocator(65536, 65536, 65536))//
					.childHandler(new ChannelInitializer<SocketChannel>() {

						@Override
						protected void initChannel(SocketChannel ch) throws Exception {
							ch.pipeline()//
									.addLast(new IdleStateHandler(300, 300, 300))//
									.addLast(interceptor)//
									.addLast(new ChannelA());
						}

					});
			ChannelFuture cf = serverBootstrap.bind(8888).sync();
			System.out.println("开始监听");
			cf.channel().closeFuture().sync();
		} finally {
			group.shutdownGracefully();
		}

	}

	// 客户端与代理连接处理
	@Sharable
	static class IPInterceptor extends ChannelInboundHandlerAdapter {
		@Override
		public void channelActive(ChannelHandlerContext ctx) throws Exception {
			InetSocketAddress remoteAddress = (InetSocketAddress) ctx.channel().remoteAddress();
			String clientIp = remoteAddress.getAddress().getHostAddress();
			if (whiteList.contains(clientIp)) {
				// logger.debug("客户端ip地址{},通过", clientIp);
				ctx.fireChannelActive();
			} else {
				logger.error("客户端ip地址{},不在白名单内", clientIp);
				ctx.close();
			}
		}

	}

	// 客户端与代理连接处理
	@Sharable
	static class ChannelA extends ChannelInboundHandlerAdapter {
		private volatile Channel channel;
//        private volatile StringBuilder header = new StringBuilder();

		@Override
		public void channelRead(final ChannelHandlerContext ctx, Object msg) throws Exception {
			TRACEID.newTraceId();
			ByteBuf buf = (ByteBuf) msg;
			if (channel == null) {
				try {
//                    System.out.println(buf.readableBytes());
//                    header.append(buf.toString(Charset.forName("ISO-8859-1")));
					// 取巧了，netty最大传送字节数是1024，大于1024会拆包，通过判断包是否满来确认请求是否完全收到
					// 如果最后一个包刚好是1024会有问题，但是没有什么好的解决方案。
//                    if (buf.readableBytes() == 65536)
//                    {
//                        return;
//                    }
					Info info = getServer(buf.toString(Charset.forName("ISO-8859-1")));
					info.setChannel(ctx.channel());
					if (info.getServerAddr() != null) {
						System.out.println(info.getServerAddr() + "   " + info.getPort());
						channel = clientBootstrap.connect(info.getServerAddr(), info.getPort()).channel();
						channel.attr(CONTEXT).set(info);
					}
				} finally {
					buf.release();
				}
			}
			// 建立keepalive之后可能还会有请求，所以连接建立之后直接传送数据即可。
			else {
				// System.out.println(buf.toString(Charset.forName("ISO-8859-1")));
				channel.writeAndFlush(buf);
			}
		}

		@Override
		public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
			System.out.println("异常");
			if (channel != null) {
				channel.close();
			}
			ctx.close();
		}

		@Override
		public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
			System.out.println("已经很久没有A的消息了！");
			ctx.close();
			super.userEventTriggered(ctx, evt);
		}
	}

	// 代理和代理地址连接处理
	@Sharable
	static class ChannelB extends ChannelInboundHandlerAdapter {

		@Override
		public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
			ByteBuf buf = (ByteBuf) msg;
			Info info = ctx.channel().attr(CONTEXT).get();
			logger.debug("traceId:{},写{}", info.getTraceId(), buf.readableBytes());
			Channel channel = info.getChannel();
			channel.writeAndFlush(buf);
		}

		@Override
		public void channelActive(ChannelHandlerContext ctx) throws Exception {
			Info info = ctx.channel().attr(CONTEXT).get();
			if (info.getReqMethod().equals("CONNECT")) {
				String CRLF = "\r\n";
				String proxyResponse = "HTTP/1.1 200 Connection Established" + CRLF
						+ "Proxy-agent:Anyone-BlindProxyServer/1.0" + CRLF + CRLF;
				ByteBuf buffer = PooledByteBufAllocator.DEFAULT.directBuffer(proxyResponse.getBytes().length);
				buffer.writeBytes(proxyResponse.getBytes());
				logger.debug("traceId:{},连接{},{}", info.getTraceId(), info.getServerAddr(), info.getReqMethod());
				info.getChannel().writeAndFlush(buffer);
			} else {
				ByteBuf buffer = PooledByteBufAllocator.DEFAULT.directBuffer(info.getMsg().getBytes().length);
				logger.debug("traceId:{},请求,{}", info.getTraceId(), buffer.readableBytes());
				System.out.println(info.getMsg());
				ctx.writeAndFlush(buffer);
			}
		}

		@Override
		public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
			System.out.println("异常");
			ctx.close();
		}

		@Override
		public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
			System.out.println("已经很久没有B的消息了！");
			ctx.close();
			super.userEventTriggered(ctx, evt);
		}

	}

	@Data
	static class Info {
		private String serverAddr;
		private int port;
		private String reqMethod;
		private String msg;
		private Channel channel;
		private String traceId;
	}

	private static Info getServer(String header) {
		String CRLF = "\r\n";
		String serverAddr = null;
		int serverPort = 80;
		// 默认只读取前100K,假定所有的头部都不超过100K就可以解析到Host头部.这块也是不好的地方
		// 获取请求行,根据HTTP协议规范,第一行为请求行.
		String input = header;
		if (input.indexOf(CRLF) == -1) {
			Info info = new Info();
			return info;
		} else {
			Info info = new Info();
			String[] reqLine = input.split(CRLF);
			String[] params = reqLine[0].split(" ");
			String reqMethod = params[0];
			String reqUrl = params[1];
			String transferUrl = getUrl(input, reqUrl);
			if (transferUrl.indexOf(":") > 0) {
				serverAddr = transferUrl.substring(0, transferUrl.indexOf(":"));
				serverPort = Integer.parseInt(transferUrl.substring(transferUrl.indexOf(":") + 1));
			} else {
				serverAddr = transferUrl;
				serverPort = 80;
			}
			info.setTraceId(TRACEID.currentTraceId());
			info.setPort(serverPort);
			info.setServerAddr(serverAddr);
			info.setReqMethod(reqMethod);
			info.setMsg(header);
			return info;
		}
	}

	private static String getUrl(String input, String reqUrl) {
		String CRLF = "\r\n";
		String serverUrl = null;
		// 解析远端主机.
		if (reqUrl.startsWith("/")) {
			int location = 0;
			while ((location = input.indexOf(CRLF)) > 0) {
				String header = input.substring(0, location);
				// 根据HTTP协议规范,如果请求行中使用相对路径,则Web Server主机由Host头部决定.
				// 需要注意HTTP协议规范中是大小写不敏感的,所以需要使用equalsIgnoreCase
				if (header.length() > 5 && header.substring(0, 5).equalsIgnoreCase("Host:")) {
					String host = header.substring("Host:".length(), header.length());
					serverUrl = host;
					break;
				}
				input = input.substring(location + CRLF.length());
			}
		} else {
			// 根据HTTP协议规范,如果请求行中使用绝对路径,则Web
			// Server主机是URL的一部分,且必须忽略Host头部.
			serverUrl = reqUrl;
		}
		// url: http://www.baidu.com/index.html
		if (serverUrl.indexOf("//") > 0) {
			serverUrl = serverUrl.substring(serverUrl.indexOf("//") + 2);
		}
		// 去除URL中的路径只保留域名: www.baidu.com/index.html
		if (serverUrl.indexOf("/") > 0) {
			serverUrl = serverUrl.substring(0, serverUrl.indexOf("/"));
		}
		return serverUrl;
	}
}
