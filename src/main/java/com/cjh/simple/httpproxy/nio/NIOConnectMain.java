package com.cjh.simple.httpproxy.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 学习了Zookeeper的nio连接维护和处理，对nio编写代理服务器进行修改 设计思路模仿Netty, 连接器和 io处理器
 * 连接器做的工作更多，除了accept之外还需要处理第一条请求，之后转交io处理器
 * 
 *
 */
public class NIOConnectMain extends Thread
{
    private static Logger                         logger = LogManager.getLogger(LogManager.ROOT_LOGGER_NAME);
    private static Map<SocketAddress, ByteBuffer> map    = new ConcurrentHashMap<SocketAddress, ByteBuffer>();
    private static ExecutorService                pool   = Executors.newCachedThreadPool();
    static
    {
        try
        {
            Selector.open().close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    
    private final Selector      selector = Selector.open();
    private ServerSocketChannel ss;
    private NIOIOMain           nioioMain;
    
    // 标记一个Http请求结束的标志
    private byte[]              END      = new byte[] { '\r', '\n', '\r', '\n' };
    
    public NIOConnectMain(int port) throws IOException
    {
        ss = ServerSocketChannel.open();
        ss.bind(new InetSocketAddress(port));
        ss.configureBlocking(false);
        ss.register(selector, SelectionKey.OP_ACCEPT);
    }
    
    public NIOConnectMain setIoMain(NIOIOMain nioioMain)
    {
        this.nioioMain = nioioMain;
        return this;
    }
    
    @Override
    public void run()
    {
        while (ss.socket().isClosed() == false)
        {
            try
            {
                int select = selector.select(1000);
                if (select == 0)
                {
                    continue;
                }
                Set<SelectionKey> event = selector.selectedKeys();
                Iterator<SelectionKey> keyIterator = event.iterator();
                while (keyIterator.hasNext())
                {
                    SelectionKey key = keyIterator.next();
                    // 处理连接
                    if (key.isAcceptable())
                    {
                        ServerSocketChannel server = (ServerSocketChannel) key.channel();
                        SocketChannel channel = server.accept();
                        channel.configureBlocking(false);
                        channel.register(selector, SelectionKey.OP_READ);
                        SocketAddress remoteAddress = channel.getRemoteAddress();
                        map.put(remoteAddress, ByteBuffer.allocate(10240));
                    }
                    // 处理第一次读
                    else if (key.isReadable())
                    {
                        final SocketChannel channel = (SocketChannel) key.channel();
                        SocketAddress remoteAddress = channel.getRemoteAddress();
                        ByteBuffer byteBuffer = map.remove(remoteAddress);
                        if (byteBuffer == null)
                        {
                            continue;
                        }
                        channel.read(byteBuffer);
                        int position = byteBuffer.position();
                        if (position < 4)
                        {
                            continue;
                        }
                        byte[] crlf = new byte[4];
                        ByteBuffer duplicate = byteBuffer.duplicate();
                        duplicate.position(position - 4);
                        duplicate.get(crlf);
                        // if (Arrays.equals(END, crlf)) {
                        // 处理连接
                        final ConnectRequest connectRequest = analysisHttp(byteBuffer);
                        key.cancel();
                        pool.execute(new Runnable() {
                            
                            @Override
                            public void run()
                            {
                                try
                                {
                                    nioioMain.initConnect(channel, connectRequest);
                                }
                                catch (Exception e)
                                {
                                    e.printStackTrace();
                                }
                            }
                        });
                        // } else {
                        // System.out.println("啦啦啦啦啦"+byteBuffer.hasRemaining());
                        // if (byteBuffer.hasRemaining()) {
                        // // 断包，等待直到全部读取
                        // continue;
                        // } else {
                        // // 超长
                        // channel.close();
                        // }
                        // }
                    }
                    keyIterator.remove();
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }
    
    static class ConnectRequest
    {
        private String     serverAddr;
        private int        port;
        private String     reqMethod;
        private ByteBuffer context;
        
        public String getServerAddr()
        {
            return serverAddr;
        }
        
        public void setServerAddr(String serverAddr)
        {
            this.serverAddr = serverAddr;
        }
        
        public int getPort()
        {
            return port;
        }
        
        public void setPort(int port)
        {
            this.port = port;
        }
        
        public String getReqMethod()
        {
            return reqMethod;
        }
        
        public void setReqMethod(String reqMethod)
        {
            this.reqMethod = reqMethod;
        }
        
        public ByteBuffer getContext()
        {
            return context;
        }
        
        public void setContext(ByteBuffer context)
        {
            this.context = context;
        }
        
    }
    
    private static ConnectRequest analysisHttp(ByteBuffer byteBuffer)
    {
        String input = new String(byteBuffer.array());
        // System.out.println(input);
        String CRLF = "\r\n";
        String serverAddr = null;
        int serverPort = 80;
        String reqLine = input.substring(0, input.indexOf(CRLF));
        // logger.debug("traceId:{},请求：{}", TRACEID.currentTraceId(), reqLine);
        String[] params = reqLine.split(" ");
        String reqMethod = params[0];
        String reqUrl = params[1];
        String transferUrl = getUrl(input, reqUrl);
        if (transferUrl.indexOf(":") > 0)
        {
            serverAddr = transferUrl.substring(0, transferUrl.indexOf(":"));
            serverPort = Integer.parseInt(transferUrl.substring(transferUrl.indexOf(":") + 1));
        }
        else
        {
            serverAddr = transferUrl;
            serverPort = 80;
        }
        ConnectRequest info = new ConnectRequest();
        // info.setPort(60000);
        // info.setServerAddr("127.0.0.1");
        // info.setReqMethod("GET");
        info.setPort(serverPort);
        info.setServerAddr(serverAddr);
        info.setReqMethod(reqMethod);
        info.setContext(byteBuffer);
        return info;
    }
    
    private static String getUrl(String input, String reqUrl)
    {
        String CRLF = "\r\n";
        String serverUrl = null;
        // 解析远端主机.
        if (reqUrl.startsWith("/"))
        {
            int location = 0;
            while ((location = input.indexOf(CRLF)) > 0)
            {
                String header = input.substring(0, location);
                // 根据HTTP协议规范,如果请求行中使用相对路径,则Web Server主机由Host头部决定.
                // 需要注意HTTP协议规范中是大小写不敏感的,所以需要使用equalsIgnoreCase
                if (header.length() > 5 && header.substring(0, 5).equalsIgnoreCase("Host:"))
                {
                    String host = header.substring("Host:".length(), header.length());
                    serverUrl = host;
                    break;
                }
                input = input.substring(location + CRLF.length());
            }
        }
        else
        {
            // 根据HTTP协议规范,如果请求行中使用绝对路径,则Web
            // Server主机是URL的一部分,且必须忽略Host头部.
            serverUrl = reqUrl;
        }
        // url: http://www.baidu.com/index.html
        if (serverUrl.indexOf("//") > 0)
        {
            serverUrl = serverUrl.substring(serverUrl.indexOf("//") + 2);
        }
        // 去除URL中的路径只保留域名: www.baidu.com/index.html
        if (serverUrl.indexOf("/") > 0)
        {
            serverUrl = serverUrl.substring(0, serverUrl.indexOf("/"));
        }
        return serverUrl;
    }
}
