package com.cjh.simple.httpproxy.nio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class NioSender extends Thread {
	private BlockingQueue<SendPacket> queue = new LinkedBlockingQueue<>();

	static class SendPacket {
		SocketChannel sc;
		ByteBuffer byteBuffer;

		public SendPacket(SocketChannel sc, ByteBuffer byteBuffer) {
			this.sc = sc;
			this.byteBuffer = byteBuffer;
		}
	}

	public NioSender() throws IOException {
		start();
	}

	public void add(SendPacket packet) {
		queue.add(packet);
	}

	@Override
	public void run() {
		while (true) {
			try {
				SendPacket packet = queue.poll(1000, TimeUnit.MILLISECONDS);
				if (packet == null) {
					continue;
				}
				SocketChannel sc = packet.sc;
				ByteBuffer byteBuffer = packet.byteBuffer;
				if (sc.socket().isClosed()) {
					continue;
				}
				try {
					while (byteBuffer.hasRemaining()) {
						sc.write(packet.byteBuffer);
					}
				} catch (IOException e) {
					close(sc);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void close(SocketChannel socketChannel) {
		try {
			System.out.println(Thread.currentThread().getName() + "  " + socketChannel + "关闭连接");
			socketChannel.close();
		} catch (IOException e) {
			;
		}
	}

}
