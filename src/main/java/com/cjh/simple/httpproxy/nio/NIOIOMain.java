package com.cjh.simple.httpproxy.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.cjh.simple.httpproxy.nio.NIOConnectMain.ConnectRequest;
import com.cjh.simple.httpproxy.nio.NioSender.SendPacket;

/**
 * 处理B端 写 ->正常写 读 ->1.获取A端Socket写
 *
 */
public class NIOIOMain
{
    @SuppressWarnings("unused")
    private static Logger   logger     = LogManager.getLogger(LogManager.ROOT_LOGGER_NAME);
    private List<IoHandler> list       = new ArrayList<NIOIOMain.IoHandler>();
    private AtomicLong      atomicLong = new AtomicLong(0);
    private int             num        = 0;
    
    public NIOIOMain(int threadNum) throws IOException
    {
        num = threadNum;
        for (int i = 0; i < threadNum; i++)
        {
            NIOIOMain.IoHandler ioHandler = new NIOIOMain.IoHandler();
            ioHandler.setName("io-thread" + i);
            ioHandler.start();
            list.add(ioHandler);
        }
    }
    
    // 轮询获取线程
    private IoHandler get()
    {
        long index = atomicLong.getAndAdd(1) % num;
        IoHandler x = list.get((int) index);
        return x;
    }
    
    public void initConnect(SocketChannel remote, ConnectRequest request) throws Exception
    {
        get().initConnect(remote, request);
    }
    
    class IoHandler extends Thread
    {
        private final Selector selector  = Selector.open();
        private NioSender      nioSender = new NioSender();
        
        public IoHandler() throws IOException
        {
        }
        
        @Override
        public void run()
        {
            while (true)
            {
                try
                {
                    selector.select(1000);
                    Set<SelectionKey> set;
                    synchronized (this)
                    {
                        set = selector.selectedKeys();
                    }
                    ArrayList<SelectionKey> event = new ArrayList<SelectionKey>(set);
                    Collections.shuffle(event);
                    Iterator<SelectionKey> iterator = event.iterator();
                    while (iterator.hasNext())
                    {
                        SelectionKey key = iterator.next();
                        try
                        {
                            // 读
                            if (key.isValid() && key.isReadable())
                            {
                                doRead(key);
                            }
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                            key.channel().close();
                        }
                        finally
                        {
                            iterator.remove();
                        }
                    }
                }
                catch (IOException e1)
                {
                    e1.printStackTrace();
                }
            }
        }
        
        private void close(SocketChannel socketChannel)
        {
            try
            {
                System.out.println(Thread.currentThread().getName() + "  " + socketChannel + "关闭连接");
                socketChannel.close();
            }
            catch (IOException e)
            {
                ;
            }
        }
        
        private void doRead(SelectionKey key) throws IOException
        {
            ByteBuffer allocate = ByteBuffer.allocate(102400);
            SocketChannel sc = (SocketChannel) key.channel();
            SocketChannel remote = (SocketChannel) key.attachment();
            int readSize = sc.read(allocate);
            if (readSize == 0)
            {
                return;
            }
            else if (readSize == -1)
            {
                key.cancel();
                close(sc);
                return;
            }
            else
            {
                allocate.flip();
                this.write(remote, allocate);
                return;
            }
        }
        
        public void initConnect(SocketChannel remote, ConnectRequest request) throws IOException, InterruptedException
        {
            SocketChannel socketChannel = SocketChannel.open();
            socketChannel.configureBlocking(false);
            socketChannel.connect(new InetSocketAddress(request.getServerAddr(), request.getPort()));
            long connectTime = System.currentTimeMillis();
            while (socketChannel.finishConnect() == false)
            {
                Thread.sleep(100);
                if (System.currentTimeMillis() - connectTime > 10000)
                {
                    logger.error("{} 连接超时", request.getServerAddr());
                    socketChannel.close();
                    remote.close();
                    return;
                }
            }
            System.out.println("创建连接" + request.getServerAddr());
            socketChannel.socket().setTcpNoDelay(true);
            // socketChannel.socket().setSoLinger(false, -1);
            // 1.注册读事件
            synchronized (this)
            {
                selector.wakeup();
                socketChannel.register(selector, SelectionKey.OP_READ, remote);
                remote.register(selector, SelectionKey.OP_READ, socketChannel);
            }
            // 3. 根据请求类型直接响应或者透传
            if (request.getReqMethod().equalsIgnoreCase("CONNECT"))
            {
                String CRLF = "\r\n";
                String proxyResponse = "HTTP/1.1 200 Connection Established" + CRLF + "Proxy-agent:Anyone-BlindProxyServer/1.0" + CRLF + CRLF;
                ByteBuffer buf = ByteBuffer.allocate(proxyResponse.getBytes().length);
                buf.put(proxyResponse.getBytes());
                buf.flip();
                this.write(remote, buf);
            }
            else
            {
                ByteBuffer buf = request.getContext();
                buf.flip();
                this.write(socketChannel, buf);
            }
            return;
        }
        
        private void write(SocketChannel sc, ByteBuffer byteBuffer) throws IOException
        {
            nioSender.add(new SendPacket(sc, byteBuffer));
        }
    }
}
