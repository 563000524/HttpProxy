package com.cjh.simple.httpproxy.nio;

import java.io.IOException;

public class NIOStart {
	public static void main(String[] args) throws IOException {
		new NIOConnectMain(8888).setIoMain(new NIOIOMain(10)).start();
	}
}
