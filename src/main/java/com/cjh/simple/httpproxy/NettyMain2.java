package com.cjh.simple.httpproxy;

import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cjh.simple.httpproxy.util.TRACEID;

import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.util.AttributeKey;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 2018年9月17日14:25:33 尝试使用Netty方式实现代理服务器 -Dio.netty.leakDetectionLevel=paranoid
 */
public class NettyMain2 {
	private static final Logger logger = LoggerFactory.getLogger(NettyMain2.class);
	private static NioEventLoopGroup group = new NioEventLoopGroup(16);
	private static NioEventLoopGroup group2 = new NioEventLoopGroup(16);
	private static ServerBootstrap serverBootstrap = new ServerBootstrap();
	private static Bootstrap clientBootstrap = new Bootstrap();
	private static final AttributeKey<ProxyContext> PROXY_CONTEXT = AttributeKey.valueOf("context");
	private static final AttributeKey<ClientContext> CLIENT_CONTEXT = AttributeKey.valueOf("context");
	private static Set<String> whiteList = new HashSet<>(Arrays.asList("127.0.0.1", "0:0:0:0:0:0:0:1", "58.22.61.221"));

	public static void main(String[] args) throws InterruptedException {
		try {
			// final IPInterceptor interceptor = new IPInterceptor();
			clientBootstrap.group(group2).option(ChannelOption.TCP_NODELAY, true).channel(NioSocketChannel.class)//
					.option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT) //
					.option(ChannelOption.TCP_NODELAY, true)//
					// .option(ChannelOption.RCVBUF_ALLOCATOR, new
					// AdaptiveRecvByteBufAllocator(65536, 65536, 65536))//
					.handler(new ChannelInitializer<SocketChannel>() {

						@Override
						protected void initChannel(SocketChannel ch) throws Exception {
							ChannelPipeline pipeline = ch.pipeline();
							pipeline.addLast(new ChannelB());
						}

					});
			serverBootstrap.group(group, group).channel(NioServerSocketChannel.class)//
					.option(ChannelOption.SO_BACKLOG, 1024)//
					// .option(ChannelOption.RCVBUF_ALLOCATOR, new
					// AdaptiveRecvByteBufAllocator(65536, 65536, 65536))//
					.option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT) //
					.childOption(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT)//
					.childOption(ChannelOption.TCP_NODELAY, true)//
					// .childOption(ChannelOption.RCVBUF_ALLOCATOR, new
					// AdaptiveRecvByteBufAllocator(65536, 65536, 65536))//
					.childHandler(new ChannelInitializer<SocketChannel>() {

						@Override
						protected void initChannel(SocketChannel ch) throws Exception {
							ChannelPipeline pipeline = ch.pipeline();
							// pipeline.addLast(interceptor);
							pipeline.addLast("HttpRequestDecoder", new HttpRequestDecoder());
							pipeline.addLast("HttpObjectAggregator", new HttpObjectAggregator(1024 * 100));
							pipeline.addLast(new IdleStateHandler(30, 30, 30));
							pipeline.addLast(new ChannelA());
						}

					});
			ChannelFuture cf = serverBootstrap.bind(8888).sync();
			System.out.println("开始监听");
			cf.channel().closeFuture().sync();
		} finally {
			group.shutdownGracefully();
		}

	}

	// 客户端与代理连接处理
	@Sharable
	static class IPInterceptor extends ChannelInboundHandlerAdapter {
		@Override
		public void channelActive(ChannelHandlerContext ctx) throws Exception {
			InetSocketAddress remoteAddress = (InetSocketAddress) ctx.channel().remoteAddress();
			String clientIp = remoteAddress.getAddress().getHostAddress();
			if (whiteList.contains(clientIp)) {
				// logger.debug("客户端ip地址{},通过", clientIp);
				ctx.fireChannelActive();
			} else {
				System.out.println("客户端ip地址{},不在白名单内");
				logger.error("客户端ip地址{},不在白名单内", clientIp);
				ctx.close();
			}
		}

	}

	// 客户端与代理连接处理
	static class ChannelA extends ChannelInboundHandlerAdapter {
		@Override
		public void channelActive(ChannelHandlerContext ctx) throws Exception {

		}

		@Override
		public void channelRead(final ChannelHandlerContext ctx, Object msg) throws Exception {
			TRACEID.newTraceId();
			// 第一次进行请求
			if (msg instanceof FullHttpRequest) {
				Channel currentChannel = ctx.channel();
				ChannelPipeline pipeline = ctx.pipeline();
				pipeline.remove("HttpRequestDecoder");
				pipeline.remove("HttpObjectAggregator");
				FullHttpRequest fullHttpRequest = (FullHttpRequest) msg;
				connect(currentChannel, fullHttpRequest);
				fullHttpRequest.release();
			}
			// 透传
			else {
				ClientContext clientContext = ctx.channel().attr(CLIENT_CONTEXT).get();
				ByteBuf byteBuf = (ByteBuf) msg;
				logger.info("traceId:{},写{}", clientContext.getTraceId(), byteBuf.readableBytes());
				clientContext.getChannel().writeAndFlush(byteBuf);
			}
		}

		/**
		 * 连接目标地址
		 * 
		 * @param currentChannel
		 * @param fullHttpRequest
		 * @return
		 */
		private Channel connect(final Channel currentChannel, final FullHttpRequest fullHttpRequest) {
			final ProxyContext proxyContext = new ProxyContext(fullHttpRequest, currentChannel);
			ChannelFuture channelFuture = clientBootstrap.connect(proxyContext.getHost(), proxyContext.getPort());
			channelFuture.addListener(new GenericFutureListener<Future<? super Void>>() {
				@Override
				public void operationComplete(Future<? super Void> future) throws Exception {
					if (future.isSuccess() == false) {
						logger.error("traceId:{},连接失败,url:{},method:{}", proxyContext.getTraceId(),
								proxyContext.getUrl(), proxyContext.getHttpMethod());
						currentChannel.close();
					}
				}
			});
			Channel proxyChannel = channelFuture.channel();
			proxyChannel.attr(PROXY_CONTEXT).set(proxyContext);
			ClientContext clientContext = new ClientContext(fullHttpRequest, proxyChannel);
			currentChannel.attr(CLIENT_CONTEXT).set(clientContext);
			return proxyChannel;
		}

		@Override
		public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
			try {
				ClientContext clientContext = ctx.channel().attr(CLIENT_CONTEXT).get();
				ctx.close();
				if (clientContext.getChannel() != null) {
					logger.error("traceId:{},客户端异常关闭", clientContext.getTraceId(), cause);
					clientContext.getChannel().close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			ctx.fireExceptionCaught(cause);
		}

		@Override
		public void channelInactive(ChannelHandlerContext ctx) throws Exception {
			try {
				ClientContext clientContext = ctx.channel().attr(CLIENT_CONTEXT).get();
				ctx.close();
				if (clientContext != null) {
					logger.info("traceId:{},客户端关闭", clientContext.getTraceId());
					clientContext.getChannel().close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			ctx.fireChannelInactive();
		}

		@Override
		public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
			ClientContext clientContext = ctx.channel().attr(CLIENT_CONTEXT).get();
			if (clientContext != null) {
				logger.info("traceId:{},客户端超时关闭", clientContext.getTraceId());
			}
			ctx.close();
		}
	}

	public static final String CRLF = "\r\n";
	public static final String proxyResponse = "HTTP/1.1 200 Connection Established" + CRLF
			+ "Proxy-agent:Anyone-BlindProxyServer/1.0" + CRLF + CRLF;

	// 客户端与代理连接处理
	static class ChannelB extends ChannelInboundHandlerAdapter {
		@Override
		public void channelActive(ChannelHandlerContext ctx) throws Exception {
			ProxyContext proxyContext = ctx.channel().attr(PROXY_CONTEXT).get();
			Channel sourceChannel = proxyContext.getChannel();
			Channel targetChannel = ctx.channel();
			// FullHttpRequest fullHttpRequest = info.getFullHttpRequest();
			logger.info("traceId:{},连接成功,uri:{},method:{}", proxyContext.getTraceId(), proxyContext.getUrl(),
					proxyContext.getHttpMethod());
			if (proxyContext.getHttpMethod() == HttpMethod.CONNECT) {
				ByteBuf buffer = PooledByteBufAllocator.DEFAULT.directBuffer(proxyResponse.getBytes().length);
				buffer.writeBytes(proxyResponse.getBytes());
				sourceChannel.writeAndFlush(buffer);
			} else {
				String httpContent = proxyContext.getHttpContent();
				ByteBuf buffer = PooledByteBufAllocator.DEFAULT.directBuffer(httpContent.getBytes().length);
				buffer.writeBytes(httpContent.getBytes());
//				System.out.println(buffer.toString(Charset.forName("ISO-8859-1")));
				targetChannel.writeAndFlush(buffer);
			}
		}


		@Override
		public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
			ByteBuf byteBuf = (ByteBuf) msg;
			ProxyContext proxyContext = ctx.channel().attr(PROXY_CONTEXT).get();
			logger.info("traceId:{},读{}", proxyContext.getTraceId(), byteBuf.readableBytes());
			Channel sourceChannel = proxyContext.getChannel();
			sourceChannel.writeAndFlush(byteBuf);
		}

		@Override
		public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
			ProxyContext proxyContext = ctx.channel().attr(PROXY_CONTEXT).get();
			try {
				ctx.close();
				logger.error("traceId:{},服务端异常关闭", proxyContext.getTraceId(), cause);
				Channel sourceChannel = proxyContext.getChannel();
				sourceChannel.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			ctx.fireChannelInactive();
		}

		@Override
		public void channelInactive(ChannelHandlerContext ctx) throws Exception {
			ProxyContext proxyContext = ctx.channel().attr(PROXY_CONTEXT).get();
			try {
				ctx.close();
				logger.info("traceId:{},服务端关闭", proxyContext.getTraceId());
				Channel sourceChannel = proxyContext.getChannel();
				sourceChannel.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			ctx.fireChannelInactive();
		}

	}

	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	static class ClientContext {
		private String traceId;
		private String host;
		private int port;
		private String url;
		private HttpMethod httpMethod;
		private String httpContent;
		private Channel channel;

		public ClientContext(FullHttpRequest fullHttpRequest, Channel channel) {
			String host = fullHttpRequest.headers().get("Host");
			int port = 80;
			if (host.contains(":")) {
				port = Integer.parseInt(host.substring(host.indexOf(":") + 1, host.length()));
				host = host.substring(0, host.indexOf(":"));
			}
			String httpContent = recoverHttpString(fullHttpRequest);
			HttpMethod method = fullHttpRequest.method();
			try {
				this.url = URLDecoder.decode(fullHttpRequest.uri(), "utf8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			this.host = host;
			this.port = port;
			this.httpContent = httpContent;
			this.httpMethod = method;
			this.channel = channel;
			this.traceId = TRACEID.currentTraceId();
		}

	}

	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	static class ProxyContext {
		private String traceId;
		private String host;
		private int port;
		private String url;
		private HttpMethod httpMethod;
		private String httpContent;
		private Channel channel;

		public ProxyContext(FullHttpRequest fullHttpRequest, Channel channel) {
			String host = fullHttpRequest.headers().get("Host");
			int port = 80;
			if (host.contains(":")) {
				port = Integer.parseInt(host.substring(host.indexOf(":") + 1, host.length()));
				host = host.substring(0, host.indexOf(":"));
			}
			String httpContent = recoverHttpString(fullHttpRequest);
			HttpMethod method = fullHttpRequest.method();
			this.url = fullHttpRequest.uri();
			this.host = host;
			this.port = port;
			this.httpContent = httpContent;
			this.httpMethod = method;
			this.channel = channel;
			this.traceId = TRACEID.currentTraceId();
		}

	}

	/**
	 * FullHttpRequest还原成原始的请求串
	 * 
	 * @param buf
	 * @param req
	 */
	private static String recoverHttpString(FullHttpRequest req) {
		StringBuilder buf = new StringBuilder();
		buf.append(req.method());
		buf.append(' ');
		buf.append(req.uri());
		buf.append(' ');
		buf.append(req.protocolVersion());
		buf.append(CRLF);
		HttpHeaders headers = req.headers();
		for (Map.Entry<String, String> e : headers) {
			buf.append(e.getKey());
			buf.append(": ");
			buf.append(e.getValue());
			buf.append(CRLF);
		}
		HttpHeaders trailingHeaders = req.trailingHeaders();
		for (Map.Entry<String, String> e : trailingHeaders) {
			buf.append(e.getKey());
			buf.append(": ");
			buf.append(e.getValue());
			buf.append(CRLF);
		}
		if (req.content().readableBytes() == 0) {
			buf.append(CRLF);
		} else {
			buf.append(CRLF);
			buf.append(req.content().toString(Charset.forName("ISO-8859-1")));
			buf.append(CRLF);
			buf.append(CRLF);
		}
		return buf.toString();
	}
}
