package com.cjh.simple.httpproxy.util;

import java.util.UUID;

public class TRACEID
{
    private static final ThreadLocal<String> TRACEID = new ThreadLocal<String>();
    
    public static String newTraceId()
    {
        String traceId = UUID.randomUUID().toString().replace("-", "");
        TRACEID.set(traceId);
        return traceId;
    }
    
    public static String currentTraceId()
    {
        return TRACEID.get();
    }
    
    public static void bind(String traceId)
    {
        TRACEID.set(traceId);
    }
}
